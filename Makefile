build:
	docker build -t qnet:v1 .

run:
	docker run --rm --name keyworker -it -p 55554:55554 qnet:v1

shell:
	docker exec -it keyworker bash

separate_shell:
	docker run -it qnet:v1 bash