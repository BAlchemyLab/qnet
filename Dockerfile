FROM sl:7

# installs necessary dependencies
RUN yum update && yum install -y libmicrohttpd-devel \
    gcc \
    gcc-c++ \
    make \
    openssl-devel \
    curl-devel \
    libdb-devel \
    libconfig-devel

COPY . /usr/src/qnet

# compile ctapudp
WORKDIR /usr/src/qnet/ctapudp
RUN make all

# compile keyworker
WORKDIR /usr/src/qnet/keyworker
RUN make all 

# compile qcrypt
WORKDIR /usr/src/qnet/qcrypt
RUN make qcrypt 

# compile qkdemu
WORKDIR /usr/src/qnet/QKD_emulator
RUN make all

# make qnet root as working directory
WORKDIR /usr/src/qnet

ARG PORT=55554
# expose provided port
EXPOSE ${PORT}

# the keyworker executalbe is compiled in previous layers
# this will work as the entrypoint
# keyworker runs in debug mode on port 55554
CMD ["./keyworker/keyworker", "-d", "1"]
