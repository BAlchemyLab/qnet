Build
=======
`make all`

Tool for automation work with keys

Installing
=======
Please install keyworker manually by coping it into you bin directory (`/usr/local/bin`).

Usage
=======
```
./keyworker -p <port>
./keyworker -h

-p <port>: port to listen on (default 55554)
-d 1: show debug
-w <threads>: run num of threads (0 to 6)
-h: prints this help text
```

API
=======
Upload new key:
```
curl --data-ascii 5D56F45ED57EA8DC9CF62322849A36E8D563AE8C7E5A265CFD994213834B73A4 127.0.0.1:55554
```
In the response you will see the hash: `5620F62E6A143B002A123615D5A62189071152EB0B39D03D30AF494A79E5950D`

Get key by hash (add `key` before hash in the request):
```
curl --data-ascii key5620F62E6A143B002A123615D5A62189071152EB0B39D03D30AF494A79E5950D 127.0.0.1:55554
```
In the response you will see the uploaded key: `5D56F45ED57EA8DC9CF62322849A36E8D563AE8C7E5A265CFD994213834B73A4`

Get new key:
```
curl --data-ascii last 127.0.0.1:55554
```