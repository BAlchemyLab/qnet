Dependencies
======
`openssl-devel, libcurl-devel, libmcrypt-devel`


Build
======
`make all`

Installing
=======
Please install ctapudp manually by coping it into you bin directory (`/usr/local/bin`).

Usage
======
```
./ctapudp -i <ifacename> [-s|-c <serverIP>] [-p <port>] [-k <port>] [-u|-a] [-d]
./ctapudp ./ctapudp -s 0.0.0.0 -p 3443 -q 127.0.0.1 -r 55554 -i udp0 -e 100 -a 1 -d 1
./ctapudp ./ctapudp -c 0.0.0.0 -p 0 -q 192.168.1.199 -r 55554 -t 192.168.1.199 -k 3443 -i udp0 -e 100 -a 1 -d 1
./ctapudp -h

-i <ifacename>: Name of interface to use (mandatory)
-s <serverIP>|-c <serverIP>: address for udp socket bind(mandatory)
-p <port>: port to listen on
-t <ip>: ip to connect
-k <port>: port to connect
-a 1: use TAP or TUN (without parameter)
-d 1: outputs debug information while running
-e <ppk>: use aes-cbc with key per packet
-z <compress level (0-9)>: use gzip
-q <ip>: ip to connect for keys
-r <port>: port to connect for keys
-w <cores>: cores count (1-6)
-l 1: restrict key reuse
-h 1: prints this help text
```